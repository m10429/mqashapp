import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Client } from './client/client';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {

  private apiUrl = environment.apiUrl;
  httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/ld+json'}) }

  constructor(private http: HttpClient) { }


  public getClients(): Observable<Client[]> {
    return this.http.get<Client[]>(`${this.apiUrl}/client/list`);
}

public saveClient(item: any) {
  const url = this.apiUrl + '/client/add';
  return this.http.post(url, item, this.httpOptions);
}
}
