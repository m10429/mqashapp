import { HttpErrorResponse } from '@angular/common/http';
import {Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { ApiServiceService } from 'src/app/api-service.service';
import { Client } from '../client';
import { MatDialog } from '@angular/material/dialog';
import { ClientFormComponent } from '../dialog/client-form/client-form.component';

@Component({
  selector: 'app-list-client',
  templateUrl: './list-client.component.html',
  styleUrls: ['./list-client.component.css']
})
export class ListClientComponent implements OnInit {


  displayedColumns: string[] = ['id', 'nom', 'prenoms', 'lieuxDeResidence', 'nationnalite', 'action'];
  dataSource: any;

  constructor(private apiService: ApiServiceService,
    private dialog: MatDialog) { }



  ngOnInit(): void {
    this.getListClient();
  }



  getListClient(){
    this.apiService.getClients()
    .subscribe(
      (response: Client[]) => {
        this.dataSource = new MatTableDataSource<Client>(response);
      },
      (error: HttpErrorResponse) => {
        alert(error.message)
      }
    )
  }

  addNewClient(){
    const dialogRef = this.dialog.open(ClientFormComponent, {
      data: {
        action: 'add',
        client: ''
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getListClient();
    });
  }

  editClient(row?: any){
    const dialogRef = this.dialog.open(ClientFormComponent, {
      data: {
        action: 'edit',
        client: row
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getListClient();
    });
  }
}
