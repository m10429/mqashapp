
export interface Client{
  id: number;
  nom: string;
  prenoms: string;
  lieuxDeResidence: string;
  nationnalite: string;
  situationMatrimoniale: string ;
  dateCreation: Date;
  dateModification: Date;

}
