import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ApiServiceService } from 'src/app/api-service.service';
@Component({
  selector: 'app-client-form',
  templateUrl: './client-form.component.html',
  styleUrls: ['./client-form.component.css']
})
export class ClientFormComponent implements OnInit {

  formClient!: FormGroup;
  initData: any;

  constructor(
    private service: ApiServiceService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<ClientFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    console.log(this.data);
    if(this.data.action == "add"){
      this.initData = {}
      this.createForm()
    } else {
      this.initData = data.client;
      this.editForm();
    }

   }

  ngOnInit(): void {

  }


  createForm(): FormGroup {
    return this.formClient = this.fb.group({
      nom: [''],
      prenoms: [''],
      lieuxDeResidence: [''],
      nationnalite: [''],
      situationMatrimoniale: ['']
    });
  }

  editForm(): FormGroup {
    return this.formClient = this.fb.group({
      nom: [this.initData.nom],
      prenoms: [this.initData.prenoms],
      lieuxDeResidence: [this.initData.lieuxDeResidence],
      nationnalite: [this.initData.nationnalite],
      situationMatrimoniale: [this.initData.situationMatrimoniale]
    });
  }


  saveClient(){
    let data = this.formClient.value;
    console.log(data);
    this.service.saveClient(data).subscribe(
      (response) => {
        console.log(response);
        this.dialogRef.close()
      }
    )
  }
}
